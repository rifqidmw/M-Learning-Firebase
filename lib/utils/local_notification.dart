import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/material.dart';

import 'notification_navigate.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

class LocalNotification {
  static Future<void> showNotification(RemoteMessage message) async {
    // Parsing data notifikasi
    final dynamic data = message.data;

    final int idNotification = 1;

    const AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'BBD', 'Notification', 'All Notification is Here',
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker');
    const NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
        idNotification, data['title'], data['body'], platformChannelSpecifics,
        payload: message.messageType);
  }

  Future<void> notificationHandler(GlobalKey<NavigatorState> navigatorKey) async {
    // const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    final InitializationSettings initializationSettings = InitializationSettings();

    // Handling notifikasi yang di tap oleh pengguna
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
          if (payload != null) {
            NavigatorNavigate().go(navigatorKey, payload);
          }
        });
  }
}