import 'package:flutter/material.dart';

class NotificationBadge extends StatelessWidget {
  final String badge;
  final Map<String, dynamic> dataNotif;

  const NotificationBadge({@required this.badge, this.dataNotif});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60.0,
      height: 60.0,
      decoration: new BoxDecoration(
        color: Colors.red,
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
          child: Text(
            '$badge',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
