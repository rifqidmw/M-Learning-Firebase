import 'package:flutter/material.dart';
import 'package:mlearning/routing/constanta_routing.dart';

class NavigatorNavigate {
  go(GlobalKey<NavigatorState> navState, String type) {
    switch(type) {
      case 'qna':
        navState.currentState.pushNamed(qna);
        break;

      default:
        navState.currentState.pushNamed('error');
    }
  }
}