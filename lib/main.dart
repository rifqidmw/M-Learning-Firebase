import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/routing/routing.dart';
import 'package:mlearning/screens/qna/qna_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/theme.dart';
import 'package:mlearning/utils/views/notification_badge.dart';
import 'package:overlay_support/overlay_support.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //inisialisasi Firebase
  await Firebase.initializeApp();
  await FlutterDownloader.initialize();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseMessaging _messaging = FirebaseMessaging.instance;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    //saat app dibuka langsung register notifikasi
    registerNotification();
  }

  void registerNotification() async {
    //mendapatkan token untuk fcm
    _messaging.getToken().then((dataToken) {
      print('Token: $dataToken');
    }).catchError((e) {
      print(e);
    });

    //listener untuk fcm saat mendapatkan message dari fcm
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("Get Notif: ${message.data}");

      //menampilkan notifikasi
      showNotification(message);
    });

    //listener untuk fcm saat mendapatkan message saat app berada dibackground
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

    //listener setelah mendapatkan message dari fcm kemudian app dibuka
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('Notif: ${message.data}');
      //menampilkan notifikasi
      showNotification(message);
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        statusBarColor: kWhite));
    return OverlaySupport(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: theme(),
          initialRoute: splash,
          onGenerateRoute: Routes.generateRoute,
        )
    );
  }

  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }
}

Future<dynamic> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message");
  // return LocalNotification.showNotification(message);
}

//Menampilkan notifikasi
void showNotification(RemoteMessage message){
  //Overlay notification didalam app yang muncul selama 4 detik
  showOverlayNotification((context) {
    return GestureDetector(
      onTap: (){
        OverlaySupportEntry.of(context).dismiss();
        //saat notifikasi diklick maka akan menuju ke QnA screen dengan membawa data notifikasi
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => QnaScreen(
                  dataNotif: message.data,
                  fromNotif: true,
                )));
      },
      child: Card(
        margin: const EdgeInsets.symmetric(horizontal: 4),
        child: SafeArea(
          child: ListTile(
            //Menampilkan icon QnA
            leading: NotificationBadge(badge: "QnA", dataNotif: message.data,),
            //Title Notifikasi berisi pertanyaan
            title: Text("Pertanyaan: ${message.data['pertanyaan']}"),
            //subtitle notifikasi berisi jawaban
            subtitle: Text("Jawaban: ${message.data['jawaban']}"),
            //Icon x untuk menutup notifikasi
            trailing: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  OverlaySupportEntry.of(context).dismiss();
                }),
          ),
        ),
      ),
    );
  }, duration: Duration(milliseconds: 4000));
}
