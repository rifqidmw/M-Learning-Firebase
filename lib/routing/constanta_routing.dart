const splash = '/';
const onBoarding = '/onBoarding';
const login = "/login";
const home = "/home";

const quizList = "/quizList";
const qna = "/qna";
const courseList = "/courseList";
const tugas = "/tugas";

const profile = "/profile";
const about = "/about";
const help = "/help";

const test = "/test";
