import 'package:flutter/material.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/screens/about/about_screen.dart';
import 'package:mlearning/screens/course/list/course_list_screen.dart';
import 'package:mlearning/screens/help/help_screen.dart';
import 'package:mlearning/screens/home/home_screen.dart';
import 'package:mlearning/screens/login/login_screen.dart';
import 'package:mlearning/screens/onboarding/onboarding_screen.dart';
import 'package:mlearning/screens/profile/profile_screen.dart';
import 'package:mlearning/screens/qna/qna_screen.dart';
import 'package:mlearning/screens/quiz/list/quiz_list_screen.dart';
import 'package:mlearning/screens/splash/splash_screen.dart';
import 'package:mlearning/screens/test/test_page.dart';
import 'package:mlearning/screens/tugas/tugas_list_screen.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case onBoarding:
        return MaterialPageRoute(builder: (_) => OnBoardingScreen());
      case login:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case home:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case courseList:
        return MaterialPageRoute(builder: (_) => CourseListScreen());
      case profile:
        return MaterialPageRoute(builder: (_) => ProfileScreen());
      case quizList:
        return MaterialPageRoute(builder: (_) => QuizListScreen());
      case qna:
        return MaterialPageRoute(builder: (_) => QnaScreen());
      case about:
        return MaterialPageRoute(builder: (_) => AboutScreen());
      case tugas:
        return MaterialPageRoute(builder: (_) => TugasListScreen());
      case help:
        return MaterialPageRoute(builder: (_) => HelpScreen());
      case test:
        return MaterialPageRoute(builder: (_) => TestPage());
      default:
        return MaterialPageRoute(builder: (_) => SplashScreen());
    }
  }
}
