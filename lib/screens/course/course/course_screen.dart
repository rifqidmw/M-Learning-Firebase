import 'package:flutter/material.dart';
//disini menggunakan 3rd party library untuk melihat materi yang memiliki format pdf
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class CourseScreen extends StatefulWidget {
  //parameter untuk screen ini adalah dataMateri yang didapat dari list pada course_list_screen
  final Map<String, dynamic> dataMateri;
  CourseScreen({this.dataMateri});

  @override
  _CourseScreen createState() => new _CourseScreen(dataMateri: dataMateri);
}

class _CourseScreen extends State<CourseScreen> {
  //parameter
  Map<String, dynamic> dataMateri;
  _CourseScreen({this.dataMateri});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //app bar untuk menampilkan nama materi nya
        appBar: AppBar(
          title: Text(dataMateri['nama']),
        ),
        body: Center(
            //Widget untuk menampilkan pdf file dari url
            child: PDF().cachedFromUrl(
          dataMateri['url'],
          placeholder: (progress) => Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('load data...'),
              Text('$progress %'),
              CircularProgressIndicator()
            ],
          )),
          errorWidget: (error) => Center(child: Text(error.toString())),
        )));
  }
}
