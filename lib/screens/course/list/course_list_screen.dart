import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/screens/course/course/course_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CourseListScreen extends StatefulWidget {
  @override
  _CourseListScreen createState() => _CourseListScreen();
}

class _CourseListScreen extends State<CourseListScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //Appbar
        appBar: AppBar(
          title: Text("Teori"),
        ),
        body: Stack(children: [
          //Layout Linear Vertical
          Column(
            children: [
              //Memberi jarak spasi
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/theory.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            //Expanded adalah widget supaya child didalamnya menyesuaikan ukuran layar
            child:  Expanded(
              //StreamBuilder adalah widget untuk request data ke server dan menerima response dari server
              child: StreamBuilder<QuerySnapshot>(
                //disini tujuan server untuk StreamBuilder adalah ke Firestore dimana ke koleksi materi, jadi semua data di koleksi materi akan diambil
                stream: FirebaseFirestore.instance.collection('materi').snapshots(),
                //builder disini adalah menyiapkan response yang didapat dari server
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  //cek jika belum ada data maka akan menampilkan loading
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  }
                  //Setelah proses pengambilan dan penerimaan data dari server selesai kemudian menampilkan list data
                  return ListView.builder(
                    padding: EdgeInsets.all(8.0),
                    itemCount: snapshot.data.docs.length,
                    //Ini looping untuk membuat list data nya
                    itemBuilder: (BuildContext context, int index) {
                      //hasil dari looping di tiap data
                      DocumentSnapshot document = snapshot.data.docs[index];
                      //membuat tampilan untuk menampilkan data
                      return _buildListView(document);
                    },
                  );
                },
              ),
            ),
          ),
        ]));
  }

  //fungsi widget untuk membuat tampilan dari tiap data yang akan ditampilkan ke list view
  Widget _buildListView(DocumentSnapshot snapshot) {
    //data dari server
    var data = snapshot.data();
    //Widget supaya dapat di click
    return GestureDetector(
      //jika diclick akan pindah screen ke Course Screen
      onTap: () => {
        Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => CourseScreen(
                      dataMateri: data
                    )
                )
        )
      },
      //didalam Gesture Detector, memiliki child yaitu sebuah Container
      child: Container(
        width: double.infinity,
        height: getProportionateScreenHeight(62.0),
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(16.0)),
        decoration: BoxDecoration(
            color: kBackgroundCard,
            borderRadius:
            BorderRadius.circular(getProportionateScreenWidth(8.0))),
        //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(24.0)),
          child: Align(
            alignment: Alignment.centerLeft,
            //Menampilkan Text untuk data nama materi
            child: Text(
              data['nama'],
              style: TextStyle(color: kText1, fontSize: 16.0),
            ),
          ),
        ),
      ),
    );
  }
}
