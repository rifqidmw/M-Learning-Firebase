import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mlearning/api/firebase_api.dart';
import 'package:mlearning/model/firebase_file.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:show_dialog/show_dialog.dart' as dialog;

class TugasScreen extends StatefulWidget {
  final Map<String, dynamic> dataTugas;
  final String docId;

  TugasScreen({this.dataTugas, this.docId});

  @override
  _TugasScreenState createState() =>
      _TugasScreenState(dataTugas: dataTugas, docId: docId);
}

class _TugasScreenState extends State<TugasScreen> {
  Map<String, dynamic> dataTugas;
  String docId;
  _TugasScreenState({this.dataTugas, this.docId});

  List<firebase_storage.UploadTask> _uploadTasks = [];
  FirebaseFile futureFiles;
  String downloadedFile = "";
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();

    getCredential();
    initDownloadsDirectoryState();
  }

  void getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    futureFiles = await FirebaseApi.getData(dataTugas['nama_file']);

    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/${dataTugas['nama_file']}');

    if (file.existsSync()) {
      setState(() {
        downloadedFile = file.path;
      });
    }
  }

  void openFileExplorer() async {
    try {
      FilePickerResult result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc'],
      );
      File _dataFile = File(result.files.single.path);
      firebase_storage.UploadTask task = await uploadFile(_dataFile);
      if (task != null) {
        /*setState(() {
          _uploadTasks = [..._uploadTasks, task];
        });*/
        dialog.loadingDialog(context); // nilai manual
        task.whenComplete(() => updateKumpul(task.snapshot.ref));
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
  }

  Future updateKumpul(firebase_storage.Reference ref) async {
    final url = await FirebaseApi.downloadUrl(ref);
    if (url != null) {
      await FirebaseFirestore.instance
          .collection('tugas')
          .doc(docId)
          .collection('participant')
          .doc(sharedPreferences.getString(user_uid))
          .update({'kumpul': true, 'url': url});

      final snackBar = SnackBar(
        content: Text('${dataTugas['title']} berhasil diupload'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Navigator.pop(context);
      Navigator.pop(context);
    }
  }

  Future<firebase_storage.UploadTask> uploadFile(File file) async {
    if (file == null) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('No file was selected'),
      ));
      return null;
    }

    firebase_storage.UploadTask uploadTask;

    firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
        .ref()
        .child(file.path.split('/').last);

    final metadata = firebase_storage.SettableMetadata(
        contentType: 'file/pdf',
        customMetadata: {'picked-file-path': file.path});

    uploadTask = ref.putFile(File(file.path), metadata);

    return Future.value(uploadTask);
  }

  void _removeTaskAtIndex(int index) {
    setState(() {
      _uploadTasks = _uploadTasks..removeAt(index);
    });
  }

  Future<void> initDownloadsDirectoryState() async {
    try {
      await DownloadsPathProvider.downloadsDirectory;
    } on PlatformException {
      print('Could not get the downloads directory');
    }
    if (!mounted) return;
  }

  void downloadFile() async {
    var download = await FirebaseApi.downloadFile(futureFiles.ref);

    setState(() {
      downloadedFile = download;
    });

    final snackBar = SnackBar(
      content: Text('Downloaded ${futureFiles.name}'),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void checkPermission() async {
    var permission = await Permission.storage.request();

    if (permission.isGranted) {
      downloadFile();
    }

    if (permission.isDenied) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Harap permission untuk Write Storage diterima!'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(dataTugas['title']),
        ),
        //Widget untuk navigation drawer di pojok kanan atas
        body: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(24.0),
                ),
                GestureDetector(
                  onTap: () => {
                    downloadedFile != ""
                        ? OpenFile.open(downloadedFile)
                        : checkPermission()
                  },
                  child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        bottom: getProportionateScreenWidth(16.0)),
                    decoration: BoxDecoration(
                        color: kBackgroundCard,
                        borderRadius: BorderRadius.circular(
                            getProportionateScreenWidth(8.0))),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(24.0),
                          vertical: getProportionateScreenWidth(16.0)),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  dataTugas['nama_file'],
                                  style:
                                      TextStyle(color: kText2, fontSize: 12.0),
                                ),
                                Text(
                                  downloadedFile != "" ? "OPEN" : "DOWNLOAD",
                                  style:
                                      TextStyle(color: kText1, fontSize: 16.0),
                                ),
                              ],
                            ),
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(24.0),
                ),
                _uploadTasks.isNotEmpty
                    ? UploadTaskListTile(
                        task: _uploadTasks[0],
                        onDismissed: () => _removeTaskAtIndex(0),
                      )
                    : Container(),
                // widget button untuk tambahkan file
              ],
            ),
            // Menambahkan button unggah berkas
            //Layout Linear Vertical
            Column(
              children: [
                //Memberi jarak spasi
                Spacer(),
                Container(
                  color: kWhite,
                  padding: EdgeInsets.symmetric(
                      vertical: getProportionateScreenWidth(24.0)),
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints.tightFor(width: double.infinity),
                    child: ElevatedButton(
                        onPressed: () {
                          openFileExplorer();
                        },
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(EdgeInsets.all(
                                getProportionateScreenWidth(16.0))),
                            elevation: MaterialStateProperty.all(0),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(100.0),
                                        topRight: Radius.circular(8.0),
                                        bottomRight: Radius.circular(100.0),
                                        bottomLeft: Radius.circular(100.0)))),
                            backgroundColor:
                                MaterialStateProperty.all(kPrimary)),
                        child: Text(
                          "Unggah Berkas",
                          style: TextStyle(color: kWhite, fontSize: 16.0),
                        )),
                  ),
                )
              ],
            ),
          ],
        ));
  }
}

class UploadTaskListTile extends StatelessWidget {
  // ignore: public_member_api_docs
  const UploadTaskListTile({
    Key key,
    this.task,
    this.onDismissed,
    this.onDownload,
    this.onDownloadLink,
  }) : super(key: key);

  final firebase_storage.UploadTask /*!*/ task;

  final VoidCallback /*!*/ onDismissed;

  final VoidCallback /*!*/ onDownload;

  final VoidCallback /*!*/ onDownloadLink;

  String _bytesTransferred(firebase_storage.TaskSnapshot snapshot) {
    return '${snapshot.bytesTransferred}/${snapshot.totalBytes}';
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<firebase_storage.TaskSnapshot>(
      stream: task.snapshotEvents,
      builder: (
        BuildContext context,
        AsyncSnapshot<firebase_storage.TaskSnapshot> asyncSnapshot,
      ) {
        Widget subtitle = const Text('---');
        firebase_storage.TaskSnapshot snapshot = asyncSnapshot.data;
        firebase_storage.TaskState state = snapshot?.state;

        if (asyncSnapshot.hasError) {
          if (asyncSnapshot.error is FirebaseException &&
              (asyncSnapshot.error as FirebaseException).code == 'canceled') {
            subtitle = const Text('Upload canceled.');
          } else {
            // ignore: avoid_print
            print(asyncSnapshot.error);
            subtitle = const Text('Something went wrong.');
          }
        } else if (snapshot != null) {
          subtitle = Text('$state: ${_bytesTransferred(snapshot)} bytes sent');
        }

        return Dismissible(
          key: Key(task.hashCode.toString()),
          onDismissed: ($) => onDismissed(),
          child: ListTile(
            title: Text('Upload Task #${task.hashCode}'),
            subtitle: subtitle,
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (state == firebase_storage.TaskState.running)
                  IconButton(
                    icon: const Icon(Icons.pause),
                    onPressed: task.pause,
                  ),
                if (state == firebase_storage.TaskState.running)
                  IconButton(
                    icon: const Icon(Icons.cancel),
                    onPressed: task.cancel,
                  ),
                if (state == firebase_storage.TaskState.paused)
                  IconButton(
                    icon: const Icon(Icons.file_upload),
                    onPressed: task.resume,
                  ),
                if (state == firebase_storage.TaskState.success)
                  IconButton(
                    icon: const Icon(Icons.file_download),
                    onPressed: onDownload,
                  ),
                if (state == firebase_storage.TaskState.success)
                  IconButton(
                    icon: const Icon(Icons.link),
                    onPressed: onDownloadLink,
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
