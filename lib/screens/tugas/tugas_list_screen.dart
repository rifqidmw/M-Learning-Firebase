import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/screens/tugas/tugas_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TugasListScreen extends StatefulWidget {
  @override
  _TugasListScreen createState() => _TugasListScreen();
}

class _TugasListScreen extends State<TugasListScreen> {
  SharedPreferences sharedPreferences;
  var isAvalable = false;
  Stream<QuerySnapshot> stream;

  @override
  void initState() {
    super.initState();
    stream = newStream();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  Stream<QuerySnapshot> newStream() => FirebaseFirestore.instance.collection('tugas').snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Appbar
      appBar: AppBar(
        title: Text("Tugas"),
      ),
      body: Stack(
        children: [
          //Layout Linear Vertical
          Column(
            children: [
              //Memberi jarak spasi
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/quiz.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            //Expanded adalah widget supaya child didalamnya menyesuaikan ukuran layar
            child: StreamBuilder<QuerySnapshot>(
              //disini tujuan server untuk StreamBuilder adalah ke Firestore dimana ke koleksi kuis, jadi semua data di koleksi kui akan diambil
              stream: stream,
              //builder disini adalah menyiapkan response yang didapat dari server
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                //cek jika belum ada data maka akan menampilkan loading
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                }

                //Setelah proses pengambilan dan penerimaan data dari server selesai kemudian menampilkan list data
                return ListView.builder(
                  padding: EdgeInsets.all(8.0),
                  itemCount: snapshot.data.docs.length,
                  //Ini looping untuk membuat list data nya
                  itemBuilder: (BuildContext context, int index) {
                    DocumentSnapshot document = snapshot.data.docs[index];

                    return FutureBuilder<DocumentSnapshot>(
                      future: doesNameAlreadyExist(document),
                      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot1) {  // AsyncSnapshot<Your object type>
                        if( snapshot1.connectionState == ConnectionState.waiting){
                          return Container();
                        }else{
                          if (snapshot1.hasError)
                            return Center(child: Text('Error: ${snapshot1.error}'));
                          else {
                            if (snapshot1.hasData){
                              bool isKumpul = snapshot1.data.data()['kumpul'];
                              return _buildListView(document, isKumpul);
                            } else {
                              return Container();
                            }
                          }
                        }
                      },
                    );
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Future<DocumentSnapshot> doesNameAlreadyExist(DocumentSnapshot snapshot)async{
    final QuerySnapshot result = await snapshot.reference
        .collection("participant")
        .where('uid', isEqualTo: sharedPreferences.getString(user_uid))
        .limit(1)
        .get();
    final List<DocumentSnapshot> documents = result.docs;
    return Future.value(documents[0]);
  }

  void reloadStream() async {
    setState(() {
      stream = newStream();
    });
  }

  //fungsi widget untuk membuat tampilan dari tiap data yang akan ditampilkan ke list view
  Widget _buildListView(DocumentSnapshot snapshot, bool isKumpul) {
    //data dari server
    var data = snapshot.data();

    //Widget supaya dapat di click
    return GestureDetector(
      //jika diclick akan pindah screen ke Quiz Screen
      onTap: () => {
        if (!isKumpul){
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => TugasScreen(dataTugas: data, docId: snapshot.id,))).then((value) => reloadStream())
        } else {
          toast("Anda sudah mengumpulkan tugas")
        }
      },
      //didalam Gesture Detector, memiliki child yaitu sebuah Container
      child: Container(
        width: double.infinity,
        height: getProportionateScreenHeight(62.0),
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(16.0)),
        decoration: BoxDecoration(
            color: isKumpul ? kPrimary : kBackgroundCard,
            borderRadius:
                BorderRadius.circular(getProportionateScreenWidth(8.0))),
        //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(24.0)),
          //Menampilkan Text untuk data nama quiz
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              data['title'],
              style: TextStyle(color: kText1, fontSize: 16.0),
            ),
          ),
        ),
      ),
    );
  }
}
