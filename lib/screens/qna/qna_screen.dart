import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/screens/qna/build_question_list.dart';
import 'package:mlearning/screens/qna/build_empty_qna.dart';
import 'package:mlearning/utils/size_config.dart';

class QnaScreen extends StatefulWidget {
  final Map<String, dynamic> dataNotif;
  final bool fromNotif;

  QnaScreen({this.dataNotif, this.fromNotif});

  @override
  _QnaScreenState createState() =>
      _QnaScreenState(dataNotif: dataNotif, fromNotif: fromNotif);
}

class _QnaScreenState extends State<QnaScreen> {
  //parameter untuk screen ini adalah dataNotif
  Map<String, dynamic> dataNotif;
  bool data = true, fromNotif;

  _QnaScreenState({this.dataNotif, this.fromNotif});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //Appbar
      appBar: AppBar(
        title: Text("Tanya Jawab"),
      ),
      body: Stack(
        children: [
          //Layout Linear Vertical
          Column(
            children: [
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/qna.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          //cek jika data memiliki data true maka akan menampilkan screen BuildQuestionsList dan jika data false maka akan menampilkan screen BuildEmptyQnA
          data
              ? BuildQuestionsList(
                  dataNotif: dataNotif,
                  fromNotif: fromNotif,
                )
              : BuildEmptyQnA()
        ],
      ),
    );
  }
}
