import 'package:flutter/material.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BuildQuestionsList extends StatefulWidget {
  //Parameter
  final Map<String, dynamic> dataNotif;
  final bool fromNotif;
  BuildQuestionsList({Key key, this.dataNotif, this.fromNotif})
      : super(key: key);

  @override
  _BuildQuestionsList createState() =>
      _BuildQuestionsList(dataNotif: dataNotif, fromNotif: fromNotif);
}

class _BuildQuestionsList extends State<BuildQuestionsList> {
  //parameter
  Map<String, dynamic> dataNotif;
  bool fromNotif;
  _BuildQuestionsList({this.dataNotif, this.fromNotif});

  SharedPreferences sharedPreferences;
  String newQuestion = '';

  //proses yang akan dikerjakan saat screen dibuka
  @override
  void initState() {
    super.initState();
    //memanggil fungsi getCredential
    getCredential();
  }

  //Fungsi yang akan dikerjakan saat screen dibuka
  void getCredential() async {
    //inisialisasi SharedPreferences untuk session
    sharedPreferences = await SharedPreferences.getInstance();

    //check jika data fromNotif adalah true, maka akan langsung menampilkan dialog detail dari QnA
    if (fromNotif) {
      print("data not null");
      buildShowDialog(context, dataNotif, true);
    }
  }

  //Fungsi untuk menambah data question ke Firestore
  void doAddQuestion() async {
    //inisialisasi Firestore
    final FirebaseFirestore firestore = FirebaseFirestore.instance;

    //menambah data question ke firestore ke koleksi qna dengan data uid, nama, pertanyaan, token, dan jawaban(diisi kosong)
    firestore.collection('qna').add({
      'uid': sharedPreferences.getString(user_uid),
      'nama': sharedPreferences.getString(user_nama),
      'pertanyaan': newQuestion,
      'jawaban': "",
      "createdAt": DateTime.now(),
      'token': sharedPreferences.getString(user_token)
    }).then((value) => {
          //kemudian setelah proses menambah data question akan dicek apakah data berhasil ditambah, jika berhasil maka akan menutup dialog tetapi jika gagal maka akan menampilkan peringatan berupa snackbar
          if (value != null)
            {Navigator.pop(context)}
          else
            {showSnackbar('Failed save data to db')}
        });
  }

  //Menampilkan widget snackbar
  showSnackbar(String msg) {
    final snackBar = SnackBar(content: Text(msg));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(24.0)),
      child: Stack(
        children: [
          //StreamBuilder adalah widget untuk request data ke server dan menerima response dari server
          StreamBuilder<QuerySnapshot>(
            //disini tujuan server untuk StreamBuilder adalah ke Firestore dimana ke koleksi qna, jadi semua data di koleksi qna akan diambil
            stream: FirebaseFirestore.instance
                .collection('qna')
                .orderBy("createdAt", descending: true)
                .snapshots(),
            //builder disini adalah menyiapkan response yang didapat dari server
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              //cek jika belum ada data maka akan menampilkan loading
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }
              //Setelah proses pengambilan dan penerimaan data dari server selesai kemudian menampilkan list data
              return ListView.builder(
                padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 88.0),
                itemCount: snapshot.data.docs.length,
                itemBuilder: (BuildContext context, int index) {
                  DocumentSnapshot document = snapshot.data.docs[index];
                  return _buildListView(context, document);
                },
              );
            },
          ),
          //Layout Linear Vertical
          Column(
            children: [
              //Memberi jarak spasi
              Spacer(),
              //Widget button untuk membuka modal bottom sheet untuk menambah pertanyaan
              Container(
                color: kWhite,
                padding: EdgeInsets.symmetric(
                    vertical: getProportionateScreenWidth(24.0)),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: double.infinity),
                  child: ElevatedButton(
                      //saat button diclick maka akan menampilkan modal bottom sheet untuk menambah pertanyaan
                      onPressed: () {
                        buildShowModalBottomSheet(context);
                      },
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.all(
                              getProportionateScreenWidth(16.0))),
                          elevation: MaterialStateProperty.all(0),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(100.0),
                                      topRight: Radius.circular(8.0),
                                      bottomRight: Radius.circular(100.0),
                                      bottomLeft: Radius.circular(100.0)))),
                          backgroundColor: MaterialStateProperty.all(kPrimary)),
                      child: Text(
                        "Tambah Pertanyaan",
                        style: TextStyle(color: kWhite, fontSize: 16.0),
                      )),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  //fungsi widget untuk membuat tampilan dari tiap data yang akan ditampilkan ke list view
  Widget _buildListView(BuildContext context, DocumentSnapshot snapshot) {
    //data dari server
    var data = snapshot.data();
    //check apakah question sudah ada jawabannya atau belum
    bool isAnswer;
    if (data['jawaban'] != "") {
      isAnswer = true;
    } else {
      isAnswer = false;
    }

    //Widget supaya dapat di click
    return GestureDetector(
      //jika diclick maka akan menapilkan dialog dimana berisi detail QnA
      onTap: () {
        buildShowDialog(context, data, isAnswer);
      },
      child: Container(
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(24.0)),
        padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0), color: kBackgroundCard),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Widget Text untuk menampilkan data pertanyaan
            Text(
              data['pertanyaan'],
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  color: kText1, fontSize: 18.0, fontWeight: FontWeight.w500),
            ),
            //Widget untuk memberikan ruang kosong
            SizedBox(
              height: getProportionateScreenHeight(8.0),
            ),
            //WIdget Text untuk menampilkan data jawaban
            Text(
              //check apakah pertanyaan sudah ada jawabannya atau belum, jika belum maka text akan berisi ""
              isAnswer ? data['jawaban'] : "Belum terjawab...",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  color: isAnswer ? kText2 : kPrimary, fontSize: 16.0),
            ),
          ],
        ),
      ),
    );
  }

  //Widge untuk menampilkan modal bottom sheet untuk menambah pertanyaan
  Future buildShowModalBottomSheet(BuildContext context) {
    return showModalBottomSheet(
        // mengatifkan fungsi bottom sheet dapat digulirkan atau di scroll
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(getProportionateScreenWidth(24.0)),
                topRight: Radius.circular(getProportionateScreenWidth(24.0)))),
        builder: (context) => SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    top: getProportionateScreenWidth(24.0),
                    left: getProportionateScreenWidth(24.0),
                    right: getProportionateScreenWidth(24.0),
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Pertanyaan",
                      style: TextStyle(
                          color: kText1,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(24.0),
                    ),
                    // form untuk memasukan pertanyaan
                    Form(
                        child: TextFormField(
                      keyboardType: TextInputType.name,
                      onChanged: (val) => {newQuestion = val},
                      decoration: InputDecoration(
                        hintText: "Masukan Pertanyaan Anda",
                        filled: true,
                        fillColor: kBackgroundCard,
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(24.0),
                            vertical: getProportionateScreenWidth(16.0)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 0.0, style: BorderStyle.none),
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(8.0))),
                      ),
                    )),
                    SizedBox(
                      height: getProportionateScreenWidth(36.0),
                    ),
                    // bottom untuk mengirimkan pertanyaan
                    ConstrainedBox(
                      constraints: BoxConstraints.tightFor(
                          width: double.infinity,
                          height: getProportionateScreenHeight(72.0)),
                      child: ElevatedButton(
                          onPressed: () {
                            if (newQuestion.isNotEmpty) {
                              doAddQuestion();
                            } else {
                              showSnackbar("Isi pertanyaan terlebih dahulu!");
                            }
                          },
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all(EdgeInsets.all(
                                  getProportionateScreenWidth(16.0))),
                              elevation: MaterialStateProperty.all(0),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(100.0),
                                          topRight: Radius.circular(8.0),
                                          bottomRight: Radius.circular(100.0),
                                          bottomLeft: Radius.circular(100.0)))),
                              backgroundColor:
                                  MaterialStateProperty.all(kPrimary)),
                          child: Text(
                            "Kirim",
                            style: TextStyle(color: kWhite, fontSize: 16.0),
                          )),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(16.0),
                    ),
                  ],
                ),
              ),
            ));
  }

  //Widget untuk menampilkan dialog untuk detail QnA
  Future buildShowDialog(
      BuildContext context, Map<String, dynamic> data, bool isAnswer) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Padding(
              // memri jarak disemua sisi elemen
              padding: EdgeInsets.all(getProportionateScreenWidth(36.0)),
              child: Container(
                width:
                    double.infinity, // panjang elemen mengikuti panjang device
                decoration: BoxDecoration(
                    color: kWhite, // memberi warna text
                    borderRadius: BorderRadius.circular(
                        // membuat lengkungan pada elemen
                        getProportionateScreenWidth(24.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          blurRadius: 8, // memberi blur pada shadow
                          spreadRadius: 4, // memberi lebar blur pada shadow
                          offset: Offset(0.0,
                              0.0), // memberi arah x dan y blur pada shadow
                          color:
                              kText1.withOpacity(0.1)) // memberi warna shadow
                    ]),
                child: Padding(
                  // memri jarak disemua sisi elemen
                  padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
                  child: SingleChildScrollView(
                    child: Column(
                      // menampilkan elemen secara vertical atau kebawah
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // menampilkan text tanya jawab
                        Text("Pertanyaan",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.normal,
                                fontSize: 14.0,
                                // memberi ukuran text
                                color: kText2,
                                // memberi warna text
                                decoration: TextDecoration.none)),
                        // memberi style none atau tidak ada
                        // membuat jarak antar elemen emnggunakan sizebox
                        SizedBox(height: getProportionateScreenWidth(8.0)),
                        // menampilkan pertanyaan
                        Text(
                          // menampilkan pertanyaan
                          data['pertanyaan'],
                          style: TextStyle(
                              color: kText1,
                              // memberi warna text
                              fontSize: 18.0,
                              // memberi  ukuran text
                              decoration: TextDecoration.none,
                              // memberi dekorasi text tidak ada
                              fontFamily: 'Poppins',
                              // memberi jenis font
                              fontWeight:
                                  FontWeight.bold), // memberi ketebalan text
                        ),
                        // membuat jarak antar elemen menggunakan sizebox
                        SizedBox(
                          height: getProportionateScreenHeight(24.0),
                        ),
                        // menampilkan jawaban
                        Text("Jawaban",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.normal,
                                fontSize: 14.0,
                                // memberi ukuran text
                                color: kText2,
                                // memberi warna text
                                decoration: TextDecoration.none)),
                        // memberi dekorasi text tidak ada
                        // membuat jarak antar elemen menggunakan sizebox
                        SizedBox(height: getProportionateScreenWidth(8.0)),
                        Text(
                          // menampilkan jawaban
                          isAnswer ? data['jawaban'] : "Belum terjawab...",
                          style: TextStyle(
                              color: isAnswer ? kText1 : kPrimary,
                              // memberi warna text
                              fontFamily: 'Poppins',
                              // memberi jenis font
                              decoration: TextDecoration.none,
                              // memberi dekorasi text tidak ada
                              fontSize: 18.0,
                              // memberi ukuran text
                              fontWeight:
                                  FontWeight.bold), // memberi ketebalan text
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }
}
