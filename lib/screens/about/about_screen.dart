import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreen createState() => _AboutScreen();
}

class _AboutScreen extends State<AboutScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //App bar
      appBar: AppBar(
        title: Text("Tentang Aplikasi"),
      ),
      body: Stack(
        children: [
          //Layout Linear Vertical
          Column(
            children: [
              //Diberi jarak spasi
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/about.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            //Widget supaya layout dapat di scroll
            child: SingleChildScrollView(
              //Layout Linear Vertical
              child: Column(
                children: [
                  //Widget Text untuk menampilkan tulisan
                  Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(color: kText2, fontSize: 16.0),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
