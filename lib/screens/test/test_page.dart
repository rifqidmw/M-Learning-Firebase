import 'package:flutter/material.dart';
import 'package:mlearning/screens/test/test_screen.dart';
import 'package:overlay_support/overlay_support.dart';

class TestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
        child: MaterialApp(
          title: 'Notify',
          theme: ThemeData(
            primarySwatch: Colors.deepPurple,
          ),
          debugShowCheckedModeBanner: false,
          home: TestScreen(),
        )
    );
  }
}