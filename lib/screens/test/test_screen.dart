import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:mlearning/model/push_notification.dart';
import 'package:mlearning/screens/home/home_screen.dart';
import 'package:mlearning/utils/views/notification_badge.dart';
import 'package:overlay_support/overlay_support.dart';

class TestScreen extends StatefulWidget {
  @override
  _TestScreen createState() => _TestScreen();
}

class _TestScreen extends State<TestScreen> {
  FirebaseMessaging _messaging = FirebaseMessaging.instance;
  PushNotification _notificationInfo;
  int _totalNotifications;

  @override
  void initState() {
    _totalNotifications = 0;
    super.initState();
    registerNotification();
  }

  void registerNotification() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("Get Notif: ${message.data}");
      PushNotification notification = PushNotification.fromJson(message.data);

      setState(() {
        _notificationInfo = notification;
        _totalNotifications++;
      });

      showSimpleNotification(
        Text(_notificationInfo.title),
        leading: NotificationBadge(badge: "QnA"),
        subtitle: Text(_notificationInfo.body),
        background: Colors.cyan[700],
        duration: Duration(seconds: 2),
      );
    });

    _messaging.getToken().then((token) {
      print('Token: $token');
    }).catchError((e) {
      print(e);
    });

    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (_) => HomeScreen()));
      /*Navigator.pushNamed(context, '/message',
          arguments: MessageArguments(message, true));*/
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notify'),
        brightness: Brightness.dark,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'App for capturing Firebase Push Notifications',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 16.0),
          NotificationBadge(badge: "QnA"),
          SizedBox(height: 16.0),
          _notificationInfo != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'TITLE: ${_notificationInfo.title}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      'BODY: ${_notificationInfo.body}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message");
}
