import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoardingScreen extends StatefulWidget {
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoardingScreen>{
  SharedPreferences sharedPreferences;
  String nama;

  //proses yang akan dikerjakan saat screen dibuka
  @override
  void initState(){
    super.initState();
    //memanggil fungsi getCredential
    getCredential();
  }

  //Fungsi yang akan dikerjakan saat screen dibuka
  getCredential() async {
    //inisialisasi SharedPreferences untuk session
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      //mengambil data nama user dari shared preference/session yang kemudian disimpan ke dalam variable nama
      nama = sharedPreferences.getString(user_nama);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
      body: Padding(
        padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
        child: Center(
          //Layout Linear Vertical
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset("assets/illustrations/onboardind.svg"),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              //Widget untuk menampilkan Text tetapi memiliki fitur yang lebih banyak dibandingkan widget Text biasa, seperti dalam 1 Widget, text yg ada didalam nya dapat memiliki warna yang berbeda
              RichText(
                  //Text dengan warna hitam
                  text: TextSpan(
                      text: "Selamat Datang ",
                      style: TextStyle(
                          color: kText1, fontFamily: "Poppins", fontSize: 16.0),
                      children: [
                        //Text dengan warna orange/primary color
                        TextSpan(
                            text: nama,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontFamily: "Poppins",
                                color: kPrimary,
                                fontWeight: FontWeight.bold))
                      ])),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(36.0),
              ),
              //Widget untuk menampilkan text tentang aplikasi
              Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                textAlign: TextAlign.justify,
                style: TextStyle(color: kText2, fontSize: 16.0),
              ),

              Spacer(),
              //Widget untuk menampilkan Text tetapi memiliki fitur yang lebih banyak dibandingkan widget Text biasa, seperti dalam 1 Widget, text yg ada didalam nya dapat memiliki warna yang berbeda
              Align(
                alignment: Alignment.bottomRight,
                child: RichText(
                    text: TextSpan(
                        text: "Dari ",
                        style: TextStyle(color: kText2, fontSize: 16.0),
                        children: [
                          TextSpan(
                              text: "Infinitkode_Assets",
                              style: TextStyle(color: kPrimary, fontSize: 16.0))
                        ])),
              ),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(16.0),
              ),
              //Widget button untuk masuk ke app
              Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: double.infinity),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, home);
                      },
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.all(
                              getProportionateScreenWidth(16.0))),
                          elevation: MaterialStateProperty.all(0),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(100.0),
                                      topRight: Radius.circular(8.0),
                                      bottomRight: Radius.circular(100.0),
                                      bottomLeft: Radius.circular(100.0)))),
                          backgroundColor: MaterialStateProperty.all(kPrimary)),
                      child: Text(
                        "Mari Mulai",
                        style: TextStyle(color: kWhite, fontSize: 16.0),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
