import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<LoginScreen> {
  SharedPreferences sharedPreferences;
  FirebaseMessaging _messaging = FirebaseMessaging.instance;
  final _key = new GlobalKey<FormState>();
  String uid = '', token = '';

  //proses yang akan dikerjakan saat screen dibuka
  @override
  void initState() {
    super.initState();
    //memanggil fungsi getCredential
    getCredential();
  }

  //Fungsi yang akan dikerjakan saat screen dibuka
  void getCredential() {
    //mendapatkan token dari firebase cloud messaging
    _messaging.getToken().then((dataToken) {
      print('Token: $dataToken');
      token = dataToken;
    }).catchError((e) {
      print(e);
    });
  }

  //Fungsi untuk proses login ke app dengan mencocokkan data user yang diisi dengan data yang ada di Firestore
  void doLogin() async {
    //inisialisasi SharedPreferences untuk session
    sharedPreferences = await SharedPreferences.getInstance();

    //inisialisasi Firestore
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    var data = firestore.collection('users').doc(uid).snapshots();

    //Meencari data di firestore pada koleksi users dan document(nim) berdasarkan yg user input
    await firestore.collection('users').doc(uid).snapshots().listen((event) {
      //variable untuk ambil data
      var doc = event.data();

      //cek jika nim ada, maka selanjutnya akan mengupdate data token untuk user tersebut di Firestore
      if (doc != null) {
        FirebaseFirestore.instance
            .collection('users')
            .doc(doc['uid'])
            .update({'token': token}).then((value) => {
              //setelah berhasil update data token, kemudian menyimpan data user ke dalam SharedPreference/session
                  setState(() {
                    sharedPreferences.setString(user_uid, doc['uid']);
                    sharedPreferences.setString(user_nama, doc['nama']);
                    sharedPreferences.setString(user_role, doc['role']);
                    sharedPreferences.setString(user_token, token);
                    sharedPreferences.setBool(user_isLogin, true);
                    Navigator.pushReplacementNamed(context, onBoarding);
                  })
                });
      } else {
        //jika nim salah, maka akan menampilkan toast peringatan
        toast("NIM anda salah!!");
      }
    }).onError((e) => {print("error fetching data: $e")});
  }

  //fungsi untuk mengecek form
  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();

      if (uid.isNotEmpty) {
        doLogin();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Widget supaya layout dapat di scroll
      body: SingleChildScrollView(
        //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
        child: Padding(
          padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
          //Layout Linear Vertical
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              //Menampilkan gambar dengan format svg
              Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset("assets/illustrations/login.svg")),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(56.0),
              ),
              //Widget untuk menampilkan text NIM
              Text(
                "NIM",
                style: TextStyle(color: kText2, fontSize: 16.0),
              ),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(4.0),
              ),
              //Widget untuk menampilkan form untuk user input data nim
              Form(
                  key: _key,
                  child: TextFormField(
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tolong NIM diisi";
                        }
                      },
                      onSaved: (e) => uid = e,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(24.0)),
                          filled: true,
                          hintText: "Masukan NIM Anda",
                          hintStyle: TextStyle(color: kText2, fontSize: 14.0),
                          fillColor: kBackgroundCard,
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100.0),
                              borderSide: BorderSide(style: BorderStyle.none)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100.0),
                              borderSide:
                                  BorderSide(style: BorderStyle.none))))),
              //Widget untuk memberikan ruang kosong
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              //Widget button untuk masuk ke app
              Align(
                alignment: Alignment.bottomCenter,
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: double.infinity),
                  child: ElevatedButton(
                      //jika button diclick, akan mengecek terlebih dahulu untuk form nya yang kemudian akan melanjutkan proses login
                      onPressed: () {
                        check();
                      },
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.all(
                              getProportionateScreenWidth(16.0))),
                          elevation: MaterialStateProperty.all(0),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(100.0),
                                      topRight: Radius.circular(8.0),
                                      bottomRight: Radius.circular(100.0),
                                      bottomLeft: Radius.circular(100.0)))),
                          backgroundColor: MaterialStateProperty.all(kPrimary)),
                      child: Text(
                        "Masuk",
                        style: TextStyle(color: kWhite, fontSize: 16.0),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
