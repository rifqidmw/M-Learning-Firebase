import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/screens/score/score_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class QuizScreen extends StatefulWidget {
  //parameter
  final String idKuis, title;
  QuizScreen({this.idKuis, this.title});

  @override
  _QuizScreen createState() => new _QuizScreen(idKuis: idKuis, title: title);
}

class _QuizScreen extends State<QuizScreen> {
  //parameter
  String idKuis, title;
  _QuizScreen({this.idKuis, this.title});

  //variable untuk menyimpan data nilai
  List<int> value = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  var dataQuiz = List<QueryDocumentSnapshot>();

  //variable untuk menyimpan pilihan quiz
  String selectedChoice1;
  String selectedChoice2;
  String selectedChoice3;
  String selectedChoice4;
  String selectedChoice5;
  String selectedChoice6;
  String selectedChoice7;
  String selectedChoice8;
  String selectedChoice9;
  String selectedChoice10;
  bool dataValid = false;

  @override
  void initState(){
    super.initState();
  }

  void getDataQuiz() async{
    FirebaseFirestore.instance
        .collection("kuis")
        .doc(idKuis)
        .collection("data-kuis")
        .get()
        .then((value) => {
          value.docs.forEach((element) {
            dataQuiz.add(element);
          })
    }).then((value) => {
      setState((){
        dataQuiz = shuffle(dataQuiz);
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Appbar
      appBar: AppBar(
        title: Text(title),
      ),
      body: Stack(
        children: [
          //Layout Linear Vertical
          Column(
            children: [
              //Memberi jarak spasi
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/quiz.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          Stack(
            children: [
              //Widget supaya layout dapat di scroll
              SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(24.0)),
                    child: Container(
                      padding:
                          EdgeInsets.all(getProportionateScreenWidth(24.0)),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24.0),
                          color: kBackgroundCard),
                      //Expanded adalah widget supaya child didalamnya menyesuaikan ukuran layar
                      child: Expanded(
                        //StreamBuilder adalah widget untuk request data ke server dan menerima response dari server
                        child: _buildListView(dataQuiz)
                      ),
                    )),
              ),
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              _buttonSubmit()
            ],
          )
        ],
      ),
    );
  }

  List<QueryDocumentSnapshot> shuffle(List<QueryDocumentSnapshot> items) {
    var random = new Random();

    // Go through all elements.
    for (var i = items.length - 1; i > 0; i--) {
      // Pick a pseudorandom number according to the list length
      var n = random.nextInt(i + 1);

      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }

    return items;
  }

  Widget _buildListView(List<QueryDocumentSnapshot> snapshot) {
    //check apakah data nya memiliki jumlah data 10 atau lebih dari 10
    if (snapshot.length >= 10) {
      dataValid = true;

      //Widget supaya layout dapat di scroll
      return SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 88.0),
        child: Stack(
          children: [
            //Layout Linear Vertical
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Layout Linear Horizontal untuk menampilkan soal pertanyaan no 1
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("1. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[0].data()['pertanyaan']),
                    )
                  ],
                ),
                //Layout Linear Vertical untuk menampilkan list pilihan soal no 1
                Column(children: createRadioQuiz1(snapshot[0], 0)),

                //dan seterusnya dibawah sampai no 10
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("2. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[1].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz2(snapshot[1], 1)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("3. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[2].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz3(snapshot[2], 2)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("4. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[3].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz4(snapshot[3], 3)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("5. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[4].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz5(snapshot[4], 4)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("6. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[5].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz6(snapshot[5], 5)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("7. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[6].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz7(snapshot[6], 6)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("8. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[7].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz8(snapshot[7], 7)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("9. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[8].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz9(snapshot[8], 8)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("10. ", style: TextStyle(color: kPrimary)),
                    Expanded(
                      child: Text(snapshot[9].data()['pertanyaan']),
                    )
                  ],
                ),
                Column(children: createRadioQuiz10(snapshot[9], 9)),
              ],
            )
          ],
        ),
      );
    } else {
      dataValid = false;
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Data kuis belum mencapai 10 atau melebihi 10")
          ]);
    }
  }

  //Widget untuk button submit quiz
  Widget _buttonSubmit() {
    return Column(
      children: [
        Spacer(),
        Container(
          color: kWhite,
          padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
          child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(width: double.infinity),
            child: ElevatedButton(
                //jika button diclick maka akan mengirim data nilai
                onPressed: () {
                  //jika data valid maka akan mulai proses menghitung nilai
                  if (dataValid) {
                    int finalResult = 0;
                    for (int nilai in value) {
                      if (nilai == 1) {
                        finalResult += 10;
                      }
                    }
                    print(value);
                    //kemudian akan ganti screen ke Score Screen untuk menampilkan nilai
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (_) => ScoreScreen(
                                  score: finalResult,
                                )));
                  } else {
                    showSnackbar("Data kuis kurang dari 10 atau melebihi 10");
                  }
                },
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                        EdgeInsets.all(getProportionateScreenWidth(16.0))),
                    elevation: MaterialStateProperty.all(0),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(100.0),
                            topRight: Radius.circular(8.0),
                            bottomRight: Radius.circular(100.0),
                            bottomLeft: Radius.circular(100.0)))),
                    backgroundColor: MaterialStateProperty.all(kPrimary)),
                child: Text(
                  "Submit",
                  style: TextStyle(color: kWhite, fontSize: 16.0),
                )),
          ),
        )
      ],
    );
  }

  //WIdget untuk menampilkan radio button list pilihan quiz
  List<Widget> createRadioQuiz1(QueryDocumentSnapshot snapshot, int index) {
    //data dari server
    var data = snapshot.data();
    //data jawaban dari server
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    //loop untuk radio button list
    for (var i = 1; i < 5; i++) {
      //data pilihan untuk quiz
      var pilihan = data['pilihan$i'];
      widgets.add(
        //Radio button
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice1,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice1 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice1 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz2(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice2,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice2 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice2 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz3(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice3,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice3 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice3 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz4(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice4,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice4 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice4 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz5(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice5,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice5 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice5 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz6(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice6,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice6 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice6 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz7(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice7,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice7 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice7 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz8(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice8,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice8 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice8 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz9(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice9,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice9 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice9 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz10(QueryDocumentSnapshot snapshot, int index) {
    var data = snapshot.data();
    var jawaban = data['jawaban'];
    List<Widget> widgets = [];

    for (var i = 1; i < 5; i++) {
      var pilihan = data['pilihan$i'];
      widgets.add(
        RadioListTile(
          value: pilihan,
          groupValue: selectedChoice10,
          title: Text(pilihan),
          onChanged: (val) {
            setState(() {
              selectedChoice10 = pilihan;
              if (pilihan == jawaban) {
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice10 == pilihan,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  showSnackbar(String msg) {
    final snackBar = SnackBar(content: Text(msg));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
