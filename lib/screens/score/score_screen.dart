import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';

class ScoreScreen extends StatefulWidget {
  //Parameter
  final int score;
  const ScoreScreen({Key key, this.score}) : super(key: key);

  @override
  _ScoreScreen createState() => _ScoreScreen(score: score);
}

class _ScoreScreen extends State<ScoreScreen>{
  //Parameter
  final int score;
  _ScoreScreen({this.score});

  String textResult;

  //proses yang akan dikerjakan saat screen dibuka
  @override
  void initState(){
    super.initState();

    //cek jika nilai scroe melebihi 50 atau tidak
    if (score > 50){
      textResult = "Selamat\nNilai Anda";
    } else {
      textResult = "Yaaah, semangat belajar lagi ya! \nNilai Anda";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //AppBar
      appBar: AppBar(
        title: Text("Skor"),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Spacer(),
              //menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/quiz.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            child: Column(
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(24.0),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    textResult,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: kText1, fontSize: 24.0),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(36.0),
                ),
                //Widget untuk menampilkan text score/nilai
                Text(
                  score.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: kPrimary,
                      fontSize: 72.0,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
