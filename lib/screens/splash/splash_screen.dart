import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/screens/qna/qna_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:mlearning/utils/views/notification_badge.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences sharedPreferences;
  final int splashDelay = 2;

  void navigateToLogin() {
    Navigator.pushReplacementNamed(context, login);
  }

  void navigateToHome() {
    Navigator.pushReplacementNamed(context, home);
  }

  Timer _loadWidget(bool isLogin) {
    var _duration = Duration(seconds: splashDelay);
    if (isLogin){
      return Timer(_duration, navigateToHome);
    } else {
      return Timer(_duration, navigateToLogin);
    }
  }

  @override
  void initState() {
    super.initState();
    getCredential();

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('Notif: ${message.data}');
      showNotification(message);
    });
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      bool checkValue = sharedPreferences.getBool(user_isLogin);
      if (checkValue != null) {
        if (checkValue) {
          _loadWidget(true);
        } else {
          sharedPreferences.clear();
          _loadWidget(false);
        }
      } else {
        checkValue = false;
        _loadWidget(false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(72.0),
            ),
            SizedBox(
              width: getProportionateScreenWidth(120.0),
              child: Image.asset(
                "assets/logo/logo2.png",
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: getProportionateScreenHeight(24.0),
            ),
            Text(
              "M-Learning",
              style: TextStyle(color: kText1),
            ),
            SizedBox(
              height: getProportionateScreenHeight(60.0),
            ),
            Expanded(
              child: SvgPicture.asset(
                "assets/illustrations/splash.svg",
                width: getProportionateScreenWidth(342.0),
              ),
            ),
            SizedBox(
              height: getProportionateScreenHeight(24.0),
            ),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(kPrimary),
            ),
            SizedBox(
              height: getProportionateScreenHeight(24.0),
            ),
            Text(
              "Infinitkode_Assets",
              style: TextStyle(color: kText1),
            ),
            SizedBox(
              height: getProportionateScreenHeight(24.0),
            ),
          ],
        ),
      ),
    );
  }

  showNotification(RemoteMessage message){
    showOverlayNotification((context) {
      return GestureDetector(
        onTap: (){
          OverlaySupportEntry.of(context).dismiss();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => QnaScreen(
                    dataNotif: message.data,
                    fromNotif: true,
                  )));
        },
        child: Card(
          margin: const EdgeInsets.symmetric(horizontal: 4),
          child: SafeArea(
            child: ListTile(
              leading: NotificationBadge(badge: "QnA", dataNotif: message.data,),
              title: Text("Pertanyaan: ${message.data['pertanyaan']}"),
              subtitle: Text("Jawaban: ${message.data['jawaban']}"),
              trailing: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    OverlaySupportEntry.of(context).dismiss();
                  }),
            ),
          ),
        ),
      );
    }, duration: Duration(milliseconds: 4000));
  }
}
