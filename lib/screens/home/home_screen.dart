import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //data statis untuk list fitur yang ada
  List<Map<String, dynamic>> menus = [
    {"Materi": courseList},
    {"Kuis": quizList},
    {"Tanya Jawab": qna},
    {"Tugas": tugas}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(),
      //Widget untuk navigation drawer di pojok kanan atas
      endDrawer: Drawer(
        //didalam drawer terdapat listview
        child: ListView(
          children: [
            //Widget untuk item yang ada di listview, untuk fitur profile
            ListTile(
              onTap: () {
                Navigator.pushNamed(context, profile);
              },
              title: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(24.0)),
                child: Text(
                  "Profile",
                  style: TextStyle(
                    color: kText1,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
            //Widget untuk item yang ada di listview, untuk fitur About
            ListTile(
              onTap: () {
                Navigator.pushNamed(context, about);
              },
              title: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(24.0)),
                child: Text(
                  "Tentang Aplikasi",
                  style: TextStyle(
                    color: kText1,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.pushNamed(context, help);
              },
              title: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(24.0)),
                child: Text(
                  "Help",
                  style: TextStyle(
                    color: kText1,
                    fontSize: 18.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          //Widget Layout Linear Vertical
          Column(
            children: [
              //Widget untuk memberikan spasi
              Spacer(),
              //Menampilkan gambar dengan format svg
              SvgPicture.asset(
                "assets/illustrations/home.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          //Padding widget dimana dapat menyesuaikan ukuran padding pada widget didalam nya
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            //Widget Layout Linear Vertical
            child: Column(
              children: [
                //Secara otomatis membuat list dengan isi list nya yaitu dari data statis menus yang ada diatas
                ...List.generate(
                  menus.length,
                        (index) => GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, menus[index].values.single);
                      },
                      child: Container(
                        width: double.infinity,
                        height: getProportionateScreenHeight(62.0),
                        margin: EdgeInsets.only(
                            bottom: getProportionateScreenWidth(16.0)),
                        decoration: BoxDecoration(
                            color: kBackgroundCard,
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(8.0))),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(24.0)),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              menus[index].keys.single,
                              style: TextStyle(color: kText1, fontSize: 16.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
