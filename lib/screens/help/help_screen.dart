import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class HelpScreen extends StatefulWidget {
  @override
  _HelpScreen createState() => _HelpScreen();
}

class _HelpScreen extends State<HelpScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //app bar untuk menampilkan nama materi nya
        appBar: AppBar(
          title: Text("Help"),
        ),
        body: Center(
          //Widget untuk menampilkan pdf file dari url
            child: PDF().cachedFromUrl(
              "https://firebasestorage.googleapis.com/v0/b/skripsi-flutter-1ba18.appspot.com/o/help%20app.pdf?alt=media&token=92353200-6253-49a7-9ad1-68a7d016ffe9",
              placeholder: (progress) => Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('load data...'),
                      Text('$progress %'),
                      CircularProgressIndicator()
                    ],
                  )),
              errorWidget: (error) => Center(child: Text(error.toString())),
            )));
  }
}
