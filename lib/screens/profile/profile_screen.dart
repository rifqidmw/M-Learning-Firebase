import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/constanta_session.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreen createState() => _ProfileScreen();
}

class _ProfileScreen extends State<ProfileScreen> {
  SharedPreferences sharedPreferences;
  String nama = '', userId = '';

  //proses yang akan dikerjakan saat screen dibuka
  @override
  void initState() {
    super.initState();
    //memanggil fungsi getCredential
    getCredential();
  }

  //Fungsi yang akan dikerjakan saat screen dibuka
  void getCredential() async {
    //inisialisasi SharedPreferences untuk session
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      //mengambil data nama user dan user id/nim dari shared preference/session yang kemudian disimpan ke dalam variable nama dan userId
      nama = sharedPreferences.getString(user_nama);
      userId = sharedPreferences.getString(user_uid);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Spacer(),
              SvgPicture.asset(
                "assets/illustrations/profile.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            //Layout Linear Vertical
            child: Column(
              children: [
                //Widget untuk memberikan ruang kosong
                SizedBox(
                  height: getProportionateScreenHeight(72.0),
                ),
                //Menampilkan gambar dengan format svg
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: getProportionateScreenWidth(120.0),
                    child: Image.asset(
                      "assets/logo/logo2.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                //Widget untuk memberikan ruang kosong
                SizedBox(
                  height: getProportionateScreenHeight(24.0),
                ),
                //Widget untuk menampilkan text nama user
                Text(
                  nama,
                  style: TextStyle(color: kText1, fontWeight: FontWeight.bold),
                ),
                //Widget untuk memberikan ruang kosong
                SizedBox(
                  height: getProportionateScreenHeight(24.0),
                ),
                //Widget untuk menampilkan text userid/nim user
                Text(
                  userId,
                  style: TextStyle(color: kText1),
                ),
                Spacer(),
                //Widget button untuk logout
                Container(
                  padding: EdgeInsets.symmetric(
                      vertical: getProportionateScreenWidth(24.0)),
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints.tightFor(width: double.infinity),
                    child: ElevatedButton(
                        onPressed: () {
                          //saat button diclick, sharedpreference/session akan dihapus semua data user nya dan kemudian akan berganti screen ke login
                          sharedPreferences.clear();
                          Navigator.pushNamedAndRemoveUntil(
                              context, login, (route) => false);
                        },
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(EdgeInsets.all(
                                getProportionateScreenWidth(16.0))),
                            elevation: MaterialStateProperty.all(0),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(100.0),
                                        topRight: Radius.circular(8.0),
                                        bottomRight: Radius.circular(100.0),
                                        bottomLeft: Radius.circular(100.0)))),
                            backgroundColor:
                                MaterialStateProperty.all(kPrimary)),
                        child: Text(
                          "Keluar",
                          style: TextStyle(color: kWhite, fontSize: 16.0),
                        )),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
