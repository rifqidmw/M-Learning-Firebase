import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:mlearning/model/firebase_file.dart';
import 'package:path_provider/path_provider.dart';

class FirebaseApi {
  static Future<FirebaseFile> getData(String path) async {
    final reference = FirebaseStorage.instance.ref(path);
    final url = await reference.getDownloadURL();
    final ref = reference;
    final name = path;
    final file = FirebaseFile(ref: ref, name: name, url: url);

    return file;
  }

  static Future<String> downloadUrl(Reference ref) async {
    final url = await ref.getDownloadURL();

    return url;
  }

  static Future<String> downloadFile(Reference ref) async {
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/${ref.name}');

    await ref.writeToFile(file);

    return file.path;
  }
}