class PushNotification {
  PushNotification(
      {this.title, this.body, this.clickAction, this.pertanyaan, this.jawaban});

  String title;
  String body;
  String clickAction;
  String pertanyaan;
  String jawaban;

  factory PushNotification.fromJson(Map<String, dynamic> json) {
    return PushNotification(
        title: json["title"],
        body: json["body"],
        clickAction: json["click_action"],
        pertanyaan: json["pertanyaan"],
        jawaban: json["jawaban"]);
  }
}
